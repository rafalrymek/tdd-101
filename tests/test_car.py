import os

from src.cars import Car, CarPrice


class TestCarProperties:

    def test_car_brand_recognition(self):
        test_car = Car('Peugeot 3008 1.6 Hdi Fap 110KM 2009r. Climatronic ! Alu ! Tempomat !', '290000PLN')
        assert test_car.brand == 'Peugeot'

    def test_car_model_recognition(self):
        test_car = Car('Mercedes-Benz C-Klasse 180 Business Solution Shooting br.', '€ 24.950,00')
        assert test_car.model == 'C-Klasse'

    def test_unknown_car_model_recognition(self):
        test_car = Car('Mercedes-Benz G-Klasse 180 Business Solution Shooting br.', '€ 24.950,00')
        assert test_car.model is None

    def test_car_prices_recognition(self):
        test_car = Car('Mercedes-Benz CLA-Klasse 180 Business Solution Shooting br.', '€ 24.950,00')
        assert test_car.get_price().amount == 2495000.0


class TestCarPrice:

    def test_car_price_is_float(self):
        test_car = Car('Mercedes-Benz CLA-Klasse 180 Business Solution Shooting br.', '€ 24.950,00')
        if type(test_car.get_price().amount) != float:
            raise TypeError('Incorrect type of car price is entered')

    def test_car_price_amount_recognition(self):
        test_car = Car('2014 Audi R8 4.2 QUATTRO', 'US $75,000.00')
        test_car_price = CarPrice(7500000.0, '$').amount
        assert test_car.get_price().amount == test_car_price

    def test_car_currency_recognition(self):
        test_car = Car('Mercedes-Benz CLA-Klasse 180 Business Solution Shooting br.', '€ 24.950,00')
        test_car_currency = CarPrice(2495000.0, '€').currency
        assert test_car.get_price().currency == test_car_currency
