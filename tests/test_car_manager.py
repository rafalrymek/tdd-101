import os
from assertpy import assert_that

from src.cars import CarManager

test_csv_filepath = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'data_files/car_data.csv')


def test_car_manager_should_read_data_from_csv_file():
    test_car_manager = CarManager()
    test_car_manager.load_from_csv(test_csv_filepath)


def test_car_manager_getting_cars_list():
    test_car_manager = CarManager()
    test_car_manager.load_from_csv(test_csv_filepath)
    assert len(test_car_manager.get_cars()) == 11


def test_car_manager_getting_car_brands():
    test_car_manager = CarManager()
    test_car_manager.load_from_csv(test_csv_filepath)
    car_brands = [car_brand.brand for car_brand in test_car_manager.get_cars()]
    assert_that(car_brands).contains('Peugeot', 'Ford', 'BMW', 'Skoda', 'Mercedes-Benz', 'Audi', 'Porsche', 'Honda')


def test_car_manager_getting_car_models():
    test_car_manager = CarManager()
    test_car_manager.load_from_csv(test_csv_filepath)
    car_models = [car_model.model for car_model in test_car_manager.get_cars()]
    assert_that(car_models).contains('3008', 'Mondeo', 'X4', 'Yeti', 'CLA-Klasse', 'C-Klasse', 'A3', '911', 'Accord')
