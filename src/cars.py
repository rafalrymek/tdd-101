import csv
from typing import List


class CarPrice:
    def __init__(self, amount: float, currency: str):
        self.amount = amount
        self.currency = currency


class Car:
    car_models = {
        "BMW": ['X4'],
        "Peugeot": ['3008'],
        "Ford": ['Mondeo'],
        "Skoda": ['Yeti'],
        "Mercedes-Benz": ['CLA-Klasse', 'C-Klasse'],
        "Audi": ['A3', 'A8'],
        "Porsche": ['911'],
        "Honda": ['Accord']
    }

    def __init__(self, title_string: str, price_string: str):
        self.title_string = title_string
        self.price_string = price_string
        self.model = self.get_model()
        self.brand = self.get_brand()

    def get_price(self):
        price_txt = self.price_string
        price_special_char = ",.€ $USPLN"
        for special_char in price_special_char:
            price_txt = price_txt.replace(special_char, '')
        amount = float(price_txt)
        currency_txt = self.price_string
        currency_special_char = ",. US"
        for special_char in currency_special_char:
            currency_txt = currency_txt.replace(special_char, '')
        currency = ''.join([i for i in currency_txt if not i.isdigit()])
        return CarPrice(amount, currency)

    def get_brand(self):
        for car_brand in self.car_models.keys():
            if car_brand in self.title_string:
                return car_brand

    def get_model(self):
        brand = self.get_brand()
        for model in self.car_models[brand]:
            if model in self.title_string.split():
                return model


class CarManager:
    def __init__(self):
        self.cars = []

    def load_from_csv(self, csv_path):
        with open(csv_path) as csvfile:
            csvreader = csv.DictReader(csvfile, delimiter=';')
            for row in csvreader:
                self.cars.append(Car(row['title'], row['price']))

    def get_cars(self) -> List[Car]:
        return self.cars
